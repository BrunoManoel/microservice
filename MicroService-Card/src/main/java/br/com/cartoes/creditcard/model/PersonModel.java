package br.com.cartoes.creditcard.model;

public class PersonModel {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}